<?php include("header_home.php");?>

<div class="promo">

<div class="container">

<div class="front-search">

<h1><?php echo $Settings['home_text']?></h1>

      <form role="search" method="get" action="search.php">
         <div class="form-group">
         
         <div class="col-md-6">
            <input type="text" class="form-control input-lg" id="term" name="term" placeholder="Search"> 			
            </div><!--<div col-md-5-->
            <div class="col-md-6">
            
            <select class="form-control input-lg" id="city" name="city">
                      <option value="all">All Cities</option>
                      <?php
if($SelectCity = $mysqli->query("SELECT city_id, city FROM city")){

    while($CityRow = mysqli_fetch_array($SelectCity)){
				
?>
                      <option value="<?php echo $CityRow['city'];?>"><?php echo $CityRow['city'];?></option>
                      <?php

}

	$SelectCity->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

?>
                    </select>
             </div><!--<div col-md-5-->
         </div>  
         <div class="front-submit">                           
             <button type="submit" class="btn btn-lg btn-danger"><i class="glyphicon glyphicon-search"></i> Search</button>
         </div>
       </form>

</div><!--front-search-->
</div><!--container-->

</div><!--promo-->

<script>
$(document).ready(function()
{
$('.star-rates').raty({
	readOnly: true,
  score: function() {
    return $(this).attr('data-score');

  }
});
});
</script>

<div class="container">

<div class="page-title"><h1><h1>Featured Businesses</h1></h1></div>

<?php

if($FeatSql = $mysqli->query("SELECT * FROM business LEFT JOIN categories ON categories.cat_id=business.cid WHERE business.active=1 AND business.feat=1 ORDER BY business.biz_id DESC LIMIT 6")){

$CountFeat = mysqli_num_rows($FeatSql);	

while ($FeatRow = mysqli_fetch_array($FeatSql)){
	
	$longFeat = stripslashes($FeatRow['business_name']);
	$strFeat = strlen ($longFeat);
	if ($strFeat > 25) {
	$FeatTitle = substr($longFeat,0,23).'...';
	}else{
	$FeatTitle = $longFeat;}
	
	$FeatLink = preg_replace("![^a-z0-9]+!i", "-", $longFeat);
	$FeatLink = urlencode(strtolower($FeatLink));
	
	$FeatDescription = stripslashes($FeatRow['description']);
	$strDescription = strlen ($FeatDescription);
	if ($strDescription > 70) {
	$feDescription = substr($FeatDescription,0,67).'...';
	}else{
	$feDescription = $FeatDescription;}
	
	$FeatTel = stripslashes($FeatRow['phone']);
	$FetCity = stripslashes($FeatRow['city']);
	$FeatSite = stripslashes($FeatRow['website']);
	
	if(!empty($FeatTel)){
		$FeatTelephone = $FeatTel;
	}else{
		$FeatTelephone = "N/A";		
	}
	
	$FCName = $FeatRow['category'];
	$FCLink = preg_replace("![^a-z0-9]+!i", "-", $FCName);
	$FCLink = urlencode($FCLink);
	$FCLink = strtolower($FCLink);
	

?>

<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 col-box">
 
 <div class="grid wow fadeInUp"> 
 
 <a class="over-label" href="category-<?php echo $FeatRow['cid'];?>-<?php echo $FCLink;?>"><?php echo $FeatRow['category'];?></a>
        
        <a href="business-<?php echo $FeatRow['biz_id'];?>-<?php echo $FeatTitle;?>"><img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $FeatRow['featured_image'];?>&amp;h=300&amp;w=500&amp;q=100" alt="<?php echo $FeatTitle;?>"></a>
    
    <h2><a href="business-<?php echo $FeatRow['biz_id'];?>-<?php echo $FeatLink;?>"><?php echo $FeatTitle;?></a></h2>
    <p><?php echo $feDescription;?></p>
    
    <div class="post-info-bottom">
<div class="col-rate">    
<span class="star-rates"  data-score="<?php echo $FeatRow['avg'];?>"></span> <?php echo $FeatRow['reviews'];?> Reviews
</div>

<div class="info-row"><span class="fa fa-home"></span> <?php echo $FetCity;?></div>
<div class="info-row"><span class="fa fa-phone"></span> <?php echo $FeatTelephone;?></div>
<?php if(!empty($FeatSite)){?>
<div class="info-row"><span class="fa fa-link"></span> <a href="<?php echo $FeatSite;?>" target="_blank">Website</a></div>
<?php }else{?>
<div class="info-row"><span class="fa fa-link"></span> N/A</div>
<?php }?>
</div>
 
  </div><!-- /.grid -->  
    
</div><!-- /.col-sm-12 col-xs-12 col-md-4 col-lg-4 -->

<?php     
	}
$FeatSql->close();
}else{
     printf("There Seems to be an issue");
}
if($CountFeat==0){
?>
<div class="col-note">There is nothing to display at the moment. Please check back again.</div>
<?php }?>


</div><!--container-->

<div class="container-fluid container-color">
<div class="container">
<h1>Let Your Business Reach Thousands of New Customers </h1>

<h3>List Your Business for Free!</h3>

<a class="btn btn-danger btn-lg" href="submit">Submit Now</a> 

</div><!--container-->
</div><!--container-fluid-->

<div class="container">

<div class="page-title"><h1>Latest Businesses</h1></div>

<?php

if($PostSql = $mysqli->query("SELECT * FROM business LEFT JOIN categories ON categories.cat_id=business.cid WHERE business.active=1 ORDER BY business.biz_id DESC LIMIT 6")){

$CountRows = mysqli_num_rows($PostSql);	

while ($PostRow = mysqli_fetch_array($PostSql)){
	
	$longTitle = stripslashes($PostRow['business_name']);
	$strTitle = strlen ($longTitle);
	if ($strTitle > 25) {
	$PostTitle = substr($longTitle,0,23).'...';
	}else{
	$PostTitle = $longTitle;}
	
	$PostLink = preg_replace("![^a-z0-9]+!i", "-", $longTitle);
	$PostLink = urlencode(strtolower($PostLink));
	
	$longDescription = stripslashes($PostRow['description']);
	$strDescription = strlen ($longDescription);
	if ($strDescription > 70) {
	$Description = substr($longDescription,0,67).'...';
	}else{
	$Description = $longDescription;}
	
	$Tel = stripslashes($PostRow['phone']);
	$City = stripslashes($PostRow['city']);
	$Site = stripslashes($PostRow['website']);
	
	if(!empty($Tel)){
		$Telephone = $Tel;
	}else{
		$Telephone = "N/A";		
	}
	
	$CName = $PostRow['category'];
	$CLink = preg_replace("![^a-z0-9]+!i", "-", $CName);
	$CLink = urlencode($CLink);
	$CLink = strtolower($CLink);
	

?>

<div class="col-sm-12 col-xs-12 col-md-4 col-lg-4 col-box">
 
 <div class="grid wow fadeInUp"> 
 
 <a class="over-label" href="category-<?php echo $PostRow['cid'];?>-<?php echo $CLink;?>"><?php echo $PostRow['category'];?></a>
        
        <a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>"><img class="img-responsive" src="thumbs.php?src=http://<?php echo $SiteLink;?>/uploads/<?php echo $PostRow['featured_image'];?>&amp;h=300&amp;w=500&amp;q=100" alt="<?php echo $PostTitle;?>"></a>
    
    <h2><a href="business-<?php echo $PostRow['biz_id'];?>-<?php echo $PostLink;?>"><?php echo $PostTitle;?></a></h2>
    <p><?php echo $Description;?></p>
    
    <div class="post-info-bottom">
<div class="col-rate">    
<span class="star-rates"  data-score="<?php echo $PostRow['avg'];?>"></span> <?php echo $PostRow['reviews'];?> Reviews
</div>

<div class="info-row"><span class="fa fa-home"></span> <?php echo $City;?></div>
<div class="info-row"><span class="fa fa-phone"></span> <?php echo $Telephone;?></div>
<?php if(!empty($Site)){?>
<div class="info-row"><span class="fa fa-link"></span> <a href="<?php echo $Site;?>" target="_blank">Website</a></div>
<?php }else{?>
<div class="info-row"><span class="fa fa-link"></span> N/A</div>
<?php }?>
</div>
 
  </div><!-- /.grid -->  
    
</div><!-- /.col-sm-12 col-xs-12 col-md-4 col-lg-4 -->

<?php     
	}
$PostSql->close();
}else{
     printf("There Seems to be an issue");
}
if($CountRows==0){
?>
<div class="col-note">There is nothing to display at the moment. Please check back again.</div>
<?php }?>

</div><!--container-->

<div class="container">

<?php if(!empty($Ad2)){?>
    <div class="col-shadow col-ads-long"> <?php echo $Ad2;?> </div>
    <!--col-shadow-->
<?php }?>

</div><!--container-->

<?php include("footer.php");?>