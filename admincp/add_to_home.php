<?php
error_reporting(0);

$path = "../add_to_home/";
$actual_image_name="";
$valid_formats = array("png","PNG");
if(isset($_POST) and $_SERVER['REQUEST_METHOD'] == "POST")
{

//getExtension

function getExtension($str) 
{
    $i = strrpos($str,".");
    if (!$i) { return ""; } 
    $l = strlen($str) - $i;
    $ext = substr($str,$i+1,$l);
    return $ext;
}

//getExtension


	$imagename = $_FILES['inputfile']['name'];
	$size = $_FILES['inputfile']['size'];
					
	if(strlen($imagename))
	{
		$ext = strtolower(getExtension($imagename));
		if(in_array($ext,$valid_formats))
		{
			if($size<(1024*1024))
			{
				$actual_image_name = "icon.".$ext;
				$uploadedfile = $_FILES['inputfile']['tmp_name'];


//Compress Image 
function compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth)
{
	if($ext=="png")
	{
	$src = imagecreatefrompng($uploadedfile);
	}
																		
	list($width,$height)=getimagesize($uploadedfile);
	$newheight=($height/$width)*$newwidth;
	$tmp=imagecreatetruecolor($newwidth,$newheight);
	imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);
	//$filename = $path.$newwidth.'_'.$actual_image_name;
	$filename = $path.$newwidth.'_'.$actual_image_name;
	imagejpeg($tmp,$filename,100);
	imagedestroy($tmp);
	return $filename;
}
//Compress Image 
	
											 
				$widthArray = array(196,152,16);
				foreach($widthArray as $newwidth)
				{
				$filename=compressImage($ext,$uploadedfile,$path,$actual_image_name,$newwidth);
				
				}	
				echo "<div class='alert alert-success' role='alert'>Add to home icons updated successfully.</div>";							
				
			}
			else
			echo "<div class='alert alert-danger' role='alert'>Image file is too big. Please use a PNG file that is less then 1MB in size.</div>";					
		}
		else
		echo "<div class='alert alert-danger' role='alert'>Invalid file format. Image must be a PNG file.</div>";	
	}
	else
	echo "<div class='alert alert-danger' role='alert'>Please select image..!</div>";
	exit;
}
?>