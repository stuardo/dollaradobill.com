<?php include("header.php");?>

<section class="col-md-2">

<?php include("left_menu.php");?>
                    
</section><!--col-md-2-->

<section class="col-md-10">

<ol class="breadcrumb">
  <li>Admin CP</li>
  <li class="active">Mobile Settings</li>
</ol>

<div class="page-header">
  <h3>Mobile Settings <small>Update your mobile Settings</small></h3>
</div>

<script src="js/bootstrap-filestyle.min.js"></script>
<script type="text/javascript" src="js/jquery.form.js"></script>
<script>
$(function(){
$(":file").filestyle({iconName: "glyphicon-picture", buttonText: "Select Image"});
});
//rezie
$(document).ready(function() { 
		
            $('#inputfile').die('click').live('change', function()			{ 
			           //$("#preview").html('');
			    
				$("#imageForm").ajaxForm({target: '#output', 
				     beforeSubmit:function(){ 
				
					$("#imageloadstatus").show();
					 $("#imageloadbutton").hide();
					 }, 
					success:function(){ 
				
					 $("#imageloadstatus").hide();
					 $("#imageloadbutton").show();
					}, 
					error:function(){ 
					
					 $("#imageloadstatus").hide();
					$("#imageloadbutton").show();
					} }).submit();
					
		
			});
        });
		
		$(document).ready(function()
{
    $('#mobileForm').on('submit', function(e)
    {
        e.preventDefault();
        $('#submitButton').attr('disabled', ''); // disable upload button
        //show uploading message
        $("#output").html('<div class="alert alert-info" role="alert">Submitting.. Please wait..</div>');
		
        $(this).ajaxSubmit({
        target: '#output',
        success:  afterSuccess //call function after success
        });
    });
});
 
function afterSuccess()
{	
	 
    $('#submitButton').removeAttr('disabled'); //enable submit button
   
} 
</script>

<section class="col-md-8">

<p>Make sure to upload a square image file. Image file must be in PNG format. Image must be bigger than 200px X 200px and less then 1024px X 1024px.</p>

<div class="panel panel-default">

    <div class="panel-body">
    
 
<?php 

if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $SettingsRow = mysqli_fetch_array($SiteSettings);
	
    $SiteSettings->close();
	
}else{
    
	 printf("<div class='alert alert-danger alert-pull'>There seems to be an issue. Please Trey again</div>");
}


?>

<div id="output"></div>

<form id="imageForm" action="add_to_home.php" enctype="multipart/form-data" method="post">
     
<div id='imageloadstatus' style='display:none'><img src="images/loader.gif" alt="Uploading...."/></div>

<div class="form-group">
<label for="inputfile">Add to home image</label>
<div id='imageloadbutton'>
<input type="file" id="inputfile" name="inputfile" class="filestyle" data-iconName="glyphicon-picture" data-buttonText="Select Image">
</div>
</div>

</form>


</div>

</div><!--panel panel-default-->  


<div class="panel panel-default">

    <div class="panel-body">
    
<form id="mobileForm" action="update_mobile_settings.php" method="post">

<label for="inputAddHome">Turn on Add to Home Feature</label>
                <div class="input-group">
                    <span class="input-group-addon"><span class="fa fa-check"></span></span>
                    <select class="form-control" id="inputAddHome" name="inputAddHome">
 					 <?php if ($SettingsRow['add_to_home']==1){?>
					<option value="1">ON</option>
					<option value="0">OFF</option>
					<?php }else{?>
					<option value="0">OFF</option>
					<option value="1">ON</option>
					<?php }?>
					</select>
                    </div>



<button type="submit" id="submitButton" class="btn btn-default btn-success btn-lg pull-right">Update</button>


</form>    
    
</div>

</div><!--panel panel-default-->      

</section>

</section><!--col-md-10-->

<?php include("footer.php");?>