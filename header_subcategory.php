<?php 
session_start();

include("db.php");

//Get Site Settings

if($SiteSettings = $mysqli->query("SELECT * FROM settings WHERE id='1'")){

    $Settings = mysqli_fetch_array($SiteSettings);
	
	$SiteLink = $Settings['site_link'];
	
	$SiteTitle = $Settings['site_title'];
	
	$FaceBook = $Settings['fb_page'];
	
	$Twitter = $Settings['twitter_link'];
	
	$Pinterest = $Settings['pinterest_link'];
	
	$Gplus = $Settings['google_pluse_link'];
	
	$add_to_home = $Settings['add_to_home'];
	
	$SiteSettings->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

//Get User Info

if(isset($_SESSION['username'])){
	
$LoggedUser = $_SESSION['username'];

if($GetUser = $mysqli->query("SELECT * FROM users WHERE username='$LoggedUser'")){

    $UserInfo = mysqli_fetch_array($GetUser);
	
	$LoggedUsername = strtolower($UserInfo['username']);
	
	$LoggedUserLink   = preg_replace("![^a-z0-9]+!i", "-", $LoggedUsername);
	
	$LoggedUserLink	  = strtolower($LoggedUserLink);

	$UserId = $UserInfo['user_id'];
	
	$UserEmail = $UserInfo['email'];
		
	$GetUser->close();
	
}else{
     
	 printf("There Seems to be an issue");
	 
}

}else{

$UserId = 0;	
	
}

//Ads

if($AdsSql = $mysqli->query("SELECT * FROM advertisements WHERE id='1'")){

    $AdsRow = mysqli_fetch_array($AdsSql);
	
	$Ad1 = $AdsRow['ad1'];
	$Ad2 = $AdsRow['ad2'];
	$Ad3 = $AdsRow['ad3'];

    $AdsSql->close();

}else{
	
     printf("There Seems to be an issue");
}


$id = $mysqli->escape_string($_GET['id']);

if($CatSql = $mysqli->query("SELECT * FROM categories WHERE cat_id='$id'")){

$CatRow = mysqli_fetch_array($CatSql);
	
	$CName1 = $CatRow['category'];
	$CLink1 = preg_replace("![^a-z0-9]+!i", "-", $CName1);
	$CLink1 = urlencode($CLink1);
	$CLink1 = strtolower($CLink1);

$CatSql->close();

}else{
     printf("There Seems to be an issue");
}

$UpdateSiteViews = $mysqli->query("UPDATE settings SET site_views=site_views+1 WHERE id=1");

?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title><?php echo $CatRow['category'];?> | <?php echo $SiteTitle;?></title>
<meta name="description" content="<?php echo $CatRow['cat_description'];?>" />
<meta name="keywords" content="<?php echo $Settings['meta_keywords'];?>" />

<meta name="viewport" content="width=device-width, initial-scale=1">

<?php if($add_to_home==1){?>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="<?php echo $CatRow['category'];?> | <?php echo $SiteTitle;?>">
<!-- Add to Home -->
<link rel="shortcut icon" sizes="16x16" href="add_to_home/16_icon.png">
<link rel="shortcut icon" sizes="196x196" href="add_to_home/196_icon.png">
<link rel="apple-touch-icon-precomposed" href="add_to_home/152_icon.png">
<!-- Add to Home -->
<link href="templates/<?php echo $Settings['template'];?>/css/addtohomescreen.css" rel="stylesheet" type="text/css">
<?php }?>

<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>

<!--Facebook Meta Tags-->
<meta property="fb:app_id"          content="<?php echo $Settings['fb_app_id']; ?>" /> 
<meta property="og:url"             content="http://<?php echo $SiteLink; ?>/subcategory-<?php echo $CatRow['cat_id'];?>-<?php echo $CLink1;?>" /> 
<meta property="og:title"           content="<?php echo $CatRow['category'];?>" />
<meta property="og:description" 	content="<?php echo $CatRow['cat_description'];?>" /> 
<meta property="og:image"           content="http://<?php echo $SiteLink; ?>/images/logo.png" /> 
<!--End Facebook Meta Tags-->

<link href="templates/<?php echo $Settings['template'];?>/css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/style.css" rel="stylesheet" type="text/css">
<link href="templates/<?php echo $Settings['template'];?>/css/animate.css" rel="stylesheet" type="text/css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<script src="js/jquery.min.js"></script>	
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.raty.js"></script>
<script src="js/wow.min.js"></script>

<script>
function popup(e){var t=700;var n=400;var r=(screen.width-t)/2;var i=(screen.height-n)/2;var s="width="+t+", height="+n;s+=", top="+i+", left="+r;s+=", directories=no";s+=", location=no";s+=", menubar=no";s+=", resizable=no";s+=", scrollbars=no";s+=", status=no";s+=", toolbar=no";newwin=window.open(e,"windowname5",s);if(window.focus){newwin.focus()}return false}
$(function() {
$.fn.raty.defaults.path = 'templates/default/images';
});
new WOW().init();
</script>

<?php if($add_to_home==1){?>
<script src="js/addtohomescreen.min.js"></script>
<script>
addToHomescreen({
   startDelay: 5,
   skipFirstVisit: true,
   maxDisplayCount: 1
});

</script>
<?php }?>

</head>

<body>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=257885121076317";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div id="wrap">
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container-fluid">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://<?php echo $SiteLink; ?>"><img src="images/logo.png" class="logo" alt="<?php echo $SiteTitle;?>"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="http://<?php echo $SiteLink;?>">Home</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Browse <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="all">All Businesses</a></li>
            <li><a href="popular">popular Businesses</a></li>
            <li><a href="featured">Featured Businesses</a></li>         
          </ul>
        </li>
      
                  <?php
		

$categories = array();

$sql = "SELECT cat_id, category, parent_id FROM categories ORDER BY category";
$res = $mysqli->query($sql);
while ($row = mysqli_fetch_assoc($res)) {

	
    $parent = intval($row['parent_id']);
    if (!isset($categories[$parent])) {
        $categories[$parent] = array();
    }
    $categories[$parent][] = $row;
}


function build_categories($parent, $categories) {
    if (isset($categories[$parent]) && count($categories[$parent])) {
      
	 
		 foreach ($categories[$parent] as $category) {
			 
			 $CategoryName = $category['category'];
			 $CategoryLink = preg_replace("![^a-z0-9]+!i", "-", $CategoryName);
		     $CategoryLink = urlencode($CategoryLink);
		     $CategoryLink = strtolower($CategoryLink);
			 			 
			 if($category['parent_id']==0){
			  echo '<li><a href="category-'.$category['cat_id'].'-'.$CategoryLink.'">' .$category['category'].'</a>';
			 }else{
			  echo '<li><a href="subcategory-'.$category['cat_id'].'-'.$CategoryLink.'"><span class="fa fa-angle-double-right"></span> '.$category['category'].'</a>';		 
			 }
            echo build_categories($category['cat_id'], $categories);
			echo '</li>';
        }
		

	}
   
}				
				
?>

<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">Categories <span class="caret"></span></a>
<ul class="dropdown-menu" role="menu">			

<?php $menu = build_categories(0, $categories);?>			


</ul>
</li>

				
		</ul>	
			
       <ul class="nav navbar-nav navbar-right">
      
		<li><a href="submit">Submit</a></li>
              
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Account <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
          <?php if(!isset($_SESSION['username'])){?>
            <li><a href="login">Login</a></li>
            <li><a href="register">Register</a></li>
            <?php }else{ ?>
            <li><a href="settings">Settings</a></li>
            <li><a href="my_business">My Business</a></li>
            <li><a href="bookmarks">Bookmarks</a></li>
            <li><a href="logout">Logout</a></li>
           <?php  }?> 
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
   </div><!--container--> 
  </div><!-- /.container-fluid -->
</nav>

<div class="container-fluid search-bar">

<div class="container">

<div class="row">

<form role="search" method="get" action="search.php">
         <div class="form-group">
         
         <div class="col-md-4 col-desktop-only">
			<h2>Search Local Businesses</h2>
            </div><!--col-md-5-->
         
         <div class="col-md-4">
            <input type="text" class="form-control" id="term" name="term" placeholder="Search"> 			
            </div><!--col-md-5-->
            <div class="col-md-4">
            
            <select class="form-control" id="city" name="city">
                      <option value="all">All Cities</option>
                      <?php
if($SearchCity = $mysqli->query("SELECT city_id, city FROM city")){

    while($SearchRow = mysqli_fetch_array($SearchCity)){
				
?>
                      <option value="<?php echo $SearchRow['city'];?>"><?php echo $SearchRow['city'];?></option>
                      <?php

}

	$SearchCity->close();
	
}else{
    
	 printf("There Seems to be an issue");
}

?>
                    </select>
             </div><!--col-md-5-->
         </div>  
         <div class="col-btn">               
             <button type="submit" class="btn btn-danger btn-width"><i class="glyphicon glyphicon-search"></i> <span class="col-mobile-only">Search</span></button>
       </div><!--col-btn-->  
       </form>
</div><!--row-->       
</div><!--container-->
</div><!-- /.container-fluid -->